Display Sync Module


INTRODUCTION
=============

The Display Sync module enables synchronising of custom display modes, both form and view modes, with the default mode.

A simple checkbox has been added at the bottom of each non-default display mode edit form. Once this has been checked then any changes to the display mode can only be made via the default mode form. A warning messages is displayed to this effect.


REQUIREMENTS
============

No special requirements.


INSTALLATION
============

Install as you would normally install a contributed Drupal module. See: https://www.drupal.org/documentation/install/modules-themes/modules-8


CONFIGURATION
=============

This module has no configuration settings.


MAINTAINERS
===========

Current Maintainers:
* Michael Welford - https://www.drupal.org/u/mikejw

This project has been sponsored by:
* The University of Adelaide - https://www.drupal.org/university-of-adelaide
  Visit: http://www.adelaide.edu.au
